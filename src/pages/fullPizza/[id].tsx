import React, {useEffect} from 'react';
import Header from "../../components/Header/Header";
import {useRouter} from "next/router";
import axios from "axios";
import {toast} from "react-toastify";
//import Image from "next/image";

const FullPizzaPage: React.FC = () => {
    const router = useRouter();
    const { id } = router.query;
    const [pizza, setPizza] = React.useState<{
        imageUrl: string,
        title: string,
        price: number
    }>();


    useEffect(() => {
        async function fetchData() {
            if (id) {
                try {
                    const { data } = await axios.get(`https://64bfbe3b0d8e251fd1115834.mockapi.io/items/${id}`);
                    setPizza(data);
                } catch (error) {
                    console.error(error);
                    toast.error('Произошла ошибка');
                    router.push('/');
                }
            }
        }
        fetchData();

    }, [id]);


    if (!pizza) {
        return <>Загрузка...</>;
    }


    return (
        <div className="wrapper">
            <Header/>
            <div className='container container__cart'>
            <img src={pizza?.imageUrl} alt="Pizza" />
            <h2>{pizza?.title}</h2>
            <h2>{pizza?.price} руб</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aspernatur debitis deleniti dolor
                harum libero molestias nemo neque perspiciatis quam, quisquam quod rem sapiente voluptas voluptatibus.
                Consequatur iure maxime quas?</p>
            </div>
            <button onClick={() => router.push('/')}>Назад</button>
        </div>
    );
};

export default FullPizzaPage;
