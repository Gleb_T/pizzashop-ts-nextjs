import React from 'react';
import Header from "../components/Header/Header";


const NotFound: React.FC = () => {

    return (
        <div className="wrapper">
            <Header />
            <div className="content">
                <h3>
                    ничего не найдено. Попробуйте перезагрузить страницу
                </h3>
            </div>
        </div>
    );
}

export default NotFound;