import React, { useEffect } from 'react';

import Categories from "../components/Categories/Categories";
import Sort, {sortList} from "../components/Sort/SortPopup";
import PizzaSkeleton from "../components/PIzzaBlock/PizzaSkeleton";
import PizzaBlock from "../components/PIzzaBlock/PizzaBlock";
import Pagination from "../components/Pagination/Pagination";
import {useSelector} from "react-redux";
import {setCategoryId, setCurrentPage} from "@/redux/filter/slice";
import qs from "qs";
import {useRouter} from "next/router";
import {fetchPizzas} from "@/redux/pizza/asyncAction";
import {useAppDispatch} from "@/redux/store";
import {selectFilter} from "@/redux/filter/selector";
import {selectPizzaData} from "@/redux/pizza/selector";
import {FetchPizzaArgs} from "@/redux/pizza/types";


const Home: React.FC = () => {
    const router = useRouter()
    const dispatch = useAppDispatch()
    const isSearch = React.useRef(false)
    const isMounted = React.useRef(false)
    const { categoryId, sort, currentPage, searchValue} = useSelector(selectFilter)
    const {items, status}   = useSelector(selectPizzaData)
    const sortType = sort.sortProperty

    // если не было первого рендера не вшивать параметры в URL
    useEffect(() => {
        if (isMounted.current) {
            const queryString = qs.stringify({
                sortProperty: sort.sortProperty,
                categoryId,
                currentPage,
            })

            router.push(`?${queryString}`)
        }

        isMounted.current = true

    }, [categoryId , sortType, currentPage])

    // если был первый рендер, проверка URL параметра и сохраняем в Redux
    useEffect(() => {
        if (window.location.search) {
            const params = qs.parse(window.location.search.substring(1)) as unknown as FetchPizzaArgs;
            const sort = sortList.find(obj => obj.sortProperty === params.sortType);

            if (sort) {
                params.sortType === sort.sortProperty
            }
            dispatch(fetchPizzas({
                search: params.search,
                categoryId: Number(params.categoryId),
                currentPage: Number(params.currentPage),
                sortType: params.sortType,
            }))

            isSearch.current = true
        }
    }, []);
    

    const search = searchValue ? `search=${searchValue}` : '';

    const getPizzas = async () => {
        window.scrollTo(0, 0); // скрол вверх страницы при первом рендеринге

        dispatch(fetchPizzas({
            sortType,
            categoryId,
            search,
            currentPage,
        }))
    }


    // если был первый рендер то запрашиваем данные
    useEffect(() => {
        if (!isSearch.current) {
            getPizzas()
        }
        isSearch.current = false

    },[categoryId, sort.sortProperty, searchValue, currentPage])


    const pizzas = items.map((newObj: any) => (<PizzaBlock key={newObj.id} {...newObj} />))
    const skeleton = [...Array(3)].map((_, index) => (
        <PizzaSkeleton key={index} />
    ))

    const onChangeCategory = React.useCallback((index: number) => {
        dispatch(setCategoryId(index))
    }, []);

    const onChangePage = (page: number) => {
        dispatch(setCurrentPage(page))
    }


    return (
        <div className="container">
            <div className="content__top">
                <Categories
                    value={categoryId}
                    onChangeCategory={onChangeCategory}
                />
                <Sort value={sort} />
            </div>
            <h2 className="content__title">Все пиццы:</h2>
            {status === 'error' ? (
                <p>Ошибка получения списка пиццы, попробуйте позже</p>
            ) : (
                <div className='contentWrapper'>
                    <div className="content__items">
                        {status === 'loading' ? skeleton : pizzas}
                    </div>
                </div>
            )}
                <Pagination currentPage={currentPage} onChangePage={onChangePage}/>
        </div>
    );

}

export default Home;