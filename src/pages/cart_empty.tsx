import React from 'react';
import Header from "../components/Header/Header";
import Link from "next/link";
//import Image from "next/image";

const CartEmpty: React.FC = () => {
    return (
        <div className="wrapper">
            <Header/>
            <div className="content">
                <div className="cart cart--empty">
                    <h2>Корзина пустая <span>😕</span></h2>
                    <p>Вероятней всего, вы не заказывали ещё пиццу.<br/>Для того, чтобы заказать пиццу, перейди на главную страницу.</p>
                    <img
                        src="https://react-pizza-v2.vercel.app/static/media/empty-cart.db905d1f4b063162f25b.png" alt="Empty cart"/>
                        <Link className="button button--black" href="/"><span>Вернуться назад</span></Link>
                </div>
            </div>
        </div>
    )
}

export default CartEmpty;
