import '../app/globals.scss'
import {Provider} from "react-redux";
import {store} from "@/redux/store";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
//import Layout from "../layouts/MainLayout";


function MyApp({ Component, pageProps }) {
    return (
        <Provider store={store}>
            <Component {...pageProps} />
            <ToastContainer />
        </Provider>
    );
}

export default MyApp;