import Header from "../components/Header/Header";
import Home from "../pages/home";

const HomePage = () => {

    return (
        <main>
            <div className="wrapper">
                <Header/>
                <div className="content">
                    <Home/>
                </div>
            </div>
        </main>
    )
}

export default HomePage;
