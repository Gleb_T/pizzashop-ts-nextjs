export type FetchPizzaArgs = {
    sortType: string,
    categoryId: number,
    search: string,
    currentPage: number,
}

export type Pizza = {
    id: number,
    title: string,
    price: number,
    imageUrl: string,
    types: number[],
    sizes: number[],
    rating: number
}

export enum Status {
    LOADING = 'loading',
    SUCCESS = 'success',
    ERROR = 'error',
}

export interface PizzaSliceState {
    items: Pizza[],
    status: Status,
}