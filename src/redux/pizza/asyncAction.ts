import {createAsyncThunk} from "@reduxjs/toolkit";
import axios from "axios";
import {FetchPizzaArgs, Pizza} from "@/redux/pizza/types";

export const fetchPizzas =
    createAsyncThunk<Pizza[], FetchPizzaArgs>('pizza/fetchPizzasStatus', async (params: FetchPizzaArgs, thunkAPI) => {
        const { sortType, categoryId, search, currentPage } = params;
        const { data } = await axios.get<Pizza[]>(
            `https://64bfbe3b0d8e251fd1115834.mockapi.io/items?page=${currentPage}&limit=4&category=${
                categoryId > 0 ? `${categoryId}` : ''}&${search}&sortBy=${sortType}&order=desc`)

        if (data.length === 0) {
            return thunkAPI.rejectWithValue('массив пицц пуст')
        }

        return thunkAPI.fulfillWithValue(data)

    })