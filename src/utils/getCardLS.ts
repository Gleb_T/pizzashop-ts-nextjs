
import {calcTotalPrice} from "@/utils/calcTotalPrice";
import {CartItem} from "@/redux/cart/types";

export const getCartfromLS = (): { items: CartItem[], totalPrice: number } => {
    if (typeof window === 'undefined') {
        return {} as { items: CartItem[], totalPrice: number };
    } else {
        const data = localStorage.getItem('cart');
        const items = data ? JSON.parse(data) : [];
        const totalPrice = calcTotalPrice(items);

        return {
            items: items as CartItem[],
            totalPrice,
        }
    }
}