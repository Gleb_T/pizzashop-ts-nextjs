import React, {useState} from 'react';
import styles from './PizzaBlock.module.scss';
import {useDispatch, useSelector} from "react-redux";
import {addItem} from "@/redux/cart/slice";
import {selectCartItemById} from "@/redux/cart/selectors";
import Link from "next/link";
import {CartItem} from "@/redux/cart/types";


const typeNames = ['тонкое', 'традиционное'];

type PizzaBlockProps = {
    id: string;
    title: string;
    imageUrl: string;
    types: number[];
    sizes: number[];
    price: number;
}

const PizzaBlock: React.FC<PizzaBlockProps> = ({ title, imageUrl, types, sizes, price, id }) => {
    const dispatch = useDispatch();
    const cartItem = useSelector(selectCartItemById(id));
    const [activeType, setActiveType] = useState(0);
    const [activeSize, setActiveSize] = useState(0);

    const addedCount = cartItem ? cartItem.count : 0;

    const onClickAdd = () => {
        const item: CartItem = {
            id,
            title,
            price,
            imageUrl,
            type: typeNames[activeType],
            size: sizes[activeSize],
            count: 0,
        }
        dispatch(addItem(item))
    }

    return (
        <div className={styles.pizzaBlock} key={id}>
            <Link href={`/fullPizza/${id}`}>
                <img
                    className={styles.pizzaBlockImage}
                    src={imageUrl}
                    alt={`пицца ${title}.`}
                />
                <h4 className={styles.pizzaBlockTitle}>{title}</h4>
            </Link>
                <div className={styles.pizzaBlockSelector}>
                    <ul className={styles.pizzaBlockSelectorUl}>
                        {types && types.map((type, index) => (
                            <li
                                key={index}
                                onClick={() => setActiveType(index)}
                                className={activeType === type ? styles.pizzaBlockLiActive : styles.pizzaBlockSelectorLi}
                            >
                                {typeNames[type]}
                            </li>
                        ))}
                    </ul>
                    <ul className={styles.pizzaBlockSelectorUl}>
                        {sizes && sizes.map((size, index) => (
                          <li
                              onClick={() => setActiveSize(index)}
                              key={index}
                              className={activeSize === index ? styles.pizzaBlockLiActive : styles.pizzaBlockSelectorLi}
                          >
                              {size} см.
                          </li>
                        ))}
                    </ul>
                </div>
            <div className={styles.pizzaBlockBottom}>
                <div className={styles.pizzaBlockPrice}>от {price} ₽</div>
                {/*<div className="button button--outline button--add">*/}

                <button
                    className={`${styles.button} ${styles.buttonAdd} ${styles.buttonOutline}`}
                    onClick={onClickAdd}
                >
                    <svg
                        width="12"
                        height="12"
                        viewBox="0 0 12 12"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M10.8 4.8H7.2V1.2C7.2 0.5373 6.6627 0 6 0C5.3373 0 4.8 0.5373 4.8 1.2V4.8H1.2C0.5373 4.8 0 5.3373 0 6C0 6.6627 0.5373 7.2 1.2 7.2H4.8V10.8C4.8 11.4627 5.3373 12 6 12C6.6627 12 7.2 11.4627 7.2 10.8V7.2H10.8C11.4627 7.2 12 6.6627 12 6C12 5.3373 11.4627 4.8 10.8 4.8Z"
                            fill="white"
                        />
                    </svg>
                    <span>Добавить</span>
                    {addedCount > 0 && <i>{addedCount}</i>}
                </button>
            </div>
        </div>
    );
}

export default PizzaBlock;