import React from "react"
import ContentLoader from "react-content-loader"

const isClientRender = typeof window !== "undefined"
const id = isClientRender ? 'client-id' : 'server-id'

const PizzaSkeleton = (props) => (

    <ContentLoader
        speed={2}
        width={280}
        height={465}
        viewBox="0 0 280 465"
        backgroundColor="#f3f3f3"
        foregroundColor="#ecebeb"
        id={id}
        {...props}
    >
        <circle cx="10" cy="5" r="2"/>
        <circle cx="120" cy="120" r="120"/>
        <rect x="3" y="264" rx="10" ry="10" width="239" height="32"/>
        <rect x="98" y="281" rx="0" ry="0" width="6" height="7"/>
        <rect x="11" y="316" rx="0" ry="0" width="231" height="68"/>
        <rect x="60" y="361" rx="0" ry="0" width="25" height="7"/>
        <rect x="13" y="401" rx="0" ry="0" width="97" height="41"/>
        <rect x="133" y="402" rx="19" ry="19" width="106" height="43"/>
    </ContentLoader>
)

export default PizzaSkeleton

