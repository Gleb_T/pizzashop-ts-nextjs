import React from "react";

import styles from "./Categories.module.scss";


type CategoriesProps = {
    value: number;
    onChangeCategory: (index: number) => void;
}

const categories = ["Все", "Мясные", "Вегетарианская", "Гриль", "Острые", "Закрытые"];

const  Categories: React.FC<CategoriesProps> = React.memo(({ value, onChangeCategory } ) => {

    return (
        <div className={styles.categories}>
            <ul className={styles.categoriesUl}>
                {categories.map((categoryName, index) => {
                    return (
                        <li
                            key={index}
                            className={value === index ? styles.categoriesLiActive : styles.categoriesLi}
                            onClick={() => onChangeCategory(index)}
                        >
                            {categoryName}
                        </li>
                    )
                })}
            </ul>
        </div>
    )
})

Categories.displayName = "Categories";

export default Categories;