import React, {useCallback, useState} from 'react';

import styles from './Search.module.scss';
import Image from "next/image";
import icon from '../../assets/images/love_search.svg';
import deleteIcon from '../../assets/images/delete_icon.svg';
import debounce from "lodash.debounce";
import {useDispatch} from "react-redux";
import {setSearchValue} from "@/redux/filter/slice";

const Search: React.FC = () => {
    const dispatch = useDispatch();
    const [value, setValue] = useState<string>('');
    const inputRef = React.useRef<HTMLInputElement>(null);

    const onClickClear = () => {
        dispatch(setSearchValue(''))
        setValue('');
        inputRef.current?.focus();
    };

    const updateSearchValue = useCallback(
        debounce((string: string) => {
            dispatch(setSearchValue(string))
        }, 250),[]
    );

    const onChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue(event.target.value);
        updateSearchValue(event.target.value)
    }


    return (
        <div className={styles.search}>
            <Image className={styles.searchIcon} src={icon} alt="поиск."/>
            <input
                value={value}
                onChange={onChangeInput}
                className={styles.searchInput}
                placeholder='Поиск пиццы...'
                ref={inputRef}
            />
            {value && (
                <Image
                    className={styles.searchIconDelete}
                    src={deleteIcon} alt='очистить.'
                    onClick={onClickClear}
                />
            )}
        </div>
    )
        ;
};

export default Search;
